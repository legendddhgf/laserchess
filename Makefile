EXE = Test
MAINSRC = Main.cc
MODULES = HelloWorld
MODULESRC = ${MODULES:%=%.cc}
HEADERS = ${MODULES:%=%.h}
SRC = ${MAINSRC} ${MODULESRC}
OBJ = ${SRC:%.cc=%.o}
CFLAGS = -Wall -Werror -pedantic `pkg-config --cflags gtkmm-3.0`
LFLAGS = `pkg-config --libs gtkmm-3.0`


all: exe

exe: obj
	g++ -o ${EXE} ${OBJ} ${LFLAGS}

obj: ${SRC} ${HEADERS}
	g++ ${CFLAGS} -c ${SRC}

clean:
	rm -f ${OBJ}

spotless: clean
	rm -f ${EXE}

.PHONY: all exe obj clean spotless

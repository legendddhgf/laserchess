// File: helloworld.cc

#include "HelloWorld.h"
#include <iostream>

HelloWorld::HelloWorld() {

  m_button.set_label("Hello World");
  // Sets the border width of the window.
  m_window.set_border_width(10);
  m_window.set_default_size(800, 800);

  // When the button receives the "clicked" signal, it will call the
  // on_button_clicked() method defined below.
  // TODO: check whether "this" (set before I un-inheritanced the code)
  // should be changed to something else like *m_window
  m_button.signal_clicked().connect(sigc::mem_fun(*this,
              &HelloWorld::on_button_clicked));

  // This packs the button into the Window (a container).
  m_window.add(m_button);

  // The final step is to display this newly created widget...
  m_button.show();
}

HelloWorld::~HelloWorld()
{
}

void HelloWorld::on_button_clicked()
{
  std::cout << "Hello World" << std::endl;
}

Gtk::Window *HelloWorld::get_window() {
  return &m_window;
}

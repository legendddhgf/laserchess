// File: helloworld.h

#ifndef GTKMM_EXAMPLE_HELLOWORLD_H
#define GTKMM_EXAMPLE_HELLOWORLD_H

#include <gtkmm/button.h>
#include <gtkmm/window.h>

class HelloWorld {

public:
  HelloWorld();
  virtual ~HelloWorld();
  Gtk::Window *get_window();

protected:
  //Signal handlers:
  void on_button_clicked();

  //Member widgets:
  Gtk::Window m_window;
  Gtk::Button m_button;
};

#endif
